package pl.edu.pwsztar.chess.domain;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RookTest {

    private final RulesOfGame rook = new RulesOfGame.Rook();

    @Tag("Bishop")
    @ParameterizedTest
    @CsvSource({
            " 0,  0,  0,  10",
            "-1, -1, -3, -1",
            "-1,  4, -3,   4 ",
            " 0,  1,  0,  -1 ",
    })
    void checkCorrectMoveForBishop(int xStart, int yStart, int xStop, int yStop) {
        assertTrue(rook.isCorrectMove(new Point(xStart, yStart), new Point(xStop, yStop)));
    }

    @ParameterizedTest
    @CsvSource({
            "0,  1,  1,   -2",
            "10, 10, 10,  10"
    })
    void checkIncorrectMoveForBishop(int xStart, int yStart, int xStop, int yStop) {
        assertFalse(rook.isCorrectMove(new Point(xStart, yStart), new Point(xStop, yStop)));
    }
}
