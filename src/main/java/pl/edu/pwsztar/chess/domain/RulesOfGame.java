package pl.edu.pwsztar.chess.domain;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * source na destination w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(Point source, Point destination);

    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if((source.getX() == destination.getX() && source.getY() == destination.getY()) || (Math.abs(destination.getX() - source.getX()) !=
                    Math.abs(destination.getY() - source.getY()))) {
                return false;
            }

            return Math.abs(destination.getX() - source.getX()) ==
                    Math.abs(destination.getY() - source.getY());
        }
    }

    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if((source.getX() == destination.getX() && source.getY() == destination.getY()) || (source.getX() != destination.getX() && source.getY() != destination.getX())) {
                return false;
            }

            return (source.getX() == destination.getX() && source.getY() != destination.getX()) || (source.getX() != destination.getX() && source.getY() == destination.getX());
        }
    }

    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            if((source.getX() == destination.getX() && source.getY() == destination.getY()) || (Math.abs(destination.getX() - source.getX()) > 1 || Math.abs(destination.getY() - source.getY()) > 1)) {
                return false;
            }

            return (Math.abs(destination.getX() - source.getX()) <= 1 && Math.abs(destination.getY() - source.getY()) <= 1);
        }
    }

    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(Point source, Point destination) {
            // TODO: Prosze dokonczyc implementacje
            return true;
        }
    }


    // TODO: Prosze dokonczyc implementacje kolejnych figur szachowych: Knight, King, Queen, Rook, Pawn
    // TODO: Prosze stosowac zaproponowane nazwy klas !!!
    // TODO: Kazda klasa powinna implementowac interfejs RulesOfGame
}
